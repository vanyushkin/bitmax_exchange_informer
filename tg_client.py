import logging

from decouple import config
from telethon.sync import TelegramClient

# Use your own values from my.telegram.org
api_id = config('APP_API_ID', cast=int)
api_hash = config('APP_API_HASH')
phone = '+77012110625'
logging.basicConfig(level=logging.DEBUG)

# client = TelegramClient(phone, api_id, api_hash)
#
# client.connect()
# if not client.is_user_authorized():
#     client.send_code_request(phone)
#     client.sign_in(phone, input('Enter the code: '))

# The first parameter is the .session file name (absolute paths allowed)
with TelegramClient('+77012110625', api_id, api_hash) as client:
    # Getting information about yourself
    me = client.get_me()

    # "me" is an User object. You can pretty-print
    # any Telegram object with the "stringify" method:
    print(me.stringify())

    # When you print something, you see a representation of it.
    # You can access all attributes of Telegram objects with
    # the dot operator. For example, to get the username:
    username = me.username
    print(username)
    print(me.phone)