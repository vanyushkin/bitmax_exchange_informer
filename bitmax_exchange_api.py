import base64
import hashlib
import hmac
import json
import random
import string
from datetime import datetime
from websocket import create_connection

import requests


class BitmaxExchangeAPI:

    def uuid32(self):
        return ''.join(
            random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(32))

    def utc_timestamp(self):
        tm = datetime.utcnow().timestamp()
        return int(tm * 1e3)

    def make_auth_header(self, timestamp, api_path, api_key, secret, coid=None):
        # convert timestamp to string
        if isinstance(timestamp, bytes):
            timestamp = timestamp.decode("utf-8")
        elif isinstance(timestamp, int):
            timestamp = str(timestamp)

        if coid is None:
            msg = bytearray(f"{timestamp}+{api_path}".encode("utf-8"))
        else:
            msg = bytearray(f"{timestamp}+{api_path}+{coid}".encode("utf-8"))

        hmac_key = base64.b64decode(secret)
        signature = hmac.new(hmac_key, msg, hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest()).decode("utf-8")
        header = {
            "x-auth-key": api_key,
            "x-auth-signature": signature_b64,
            "x-auth-timestamp": timestamp,
        }

        if coid is not None:
            header["x-auth-coid"] = coid

        return header

    def POST(self, url, *args, **kwargs):
        try:
            res = requests.post(url, *args, **kwargs)
            return self.__parse_response(res)
        except requests.exceptions.ConnectionError:
            print(f"[WARN] Failed to connect {url}")
            return None
        except:
            raise


    def GET(self, url, *args, **kwargs):
        try:
            res = requests.get(url, *args, **kwargs)
            return self.__parse_response(res)
        except requests.exceptions.ConnectionError:
            print(f"[WARN] Failed to connect {url}")
            return None
        except:
            raise


    def __parse_response(self, res):
        if res is None:
            return None

        if res.status_code == 200:
            data = json.loads(res.text)
            return data
        else:
            print(f"request failed, error code = {res.status_code}")
            print(res.text)



    def user_info(self, url, api_key, secret):
        ts = self.utc_timestamp()
        headers = self.make_auth_header(ts, "user/info", api_key, secret)
        return self.GET(f"{url}/api/v1/user/info", headers=headers)


    def get_account_group(self, url, api_key, secret):
        account_group = self.user_info(url, api_key, secret)
        return account_group['accountGroup']

class BitmaxExchangeWebsocketAPI:

    def uuid32(self):
        return ''.join(
            random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(32))

    def utc_timestamp(self):
        tm = datetime.utcnow().timestamp()
        return int(tm * 1e3)

    def make_auth_header(self, timestamp, api_path, api_key, secret, coid=None):
        # convert timestamp to string
        if isinstance(timestamp, bytes):
            timestamp = timestamp.decode("utf-8")
        elif isinstance(timestamp, int):
            timestamp = str(timestamp)

        if coid is None:
            msg = bytearray(f"{timestamp}+{api_path}".encode("utf-8"))
        else:
            msg = bytearray(f"{timestamp}+{api_path}+{coid}".encode("utf-8"))

        hmac_key = base64.b64decode(secret)
        signature = hmac.new(hmac_key, msg, hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest()).decode("utf-8")
        header = {
            "x-auth-key": api_key,
            "x-auth-signature": signature_b64,
            "x-auth-timestamp": timestamp,
        }

        if coid is not None:
            header["x-auth-coid"] = coid

        return header

    def connect(self, url, api_key, secret):
        ts = self.utc_timestamp()
        headers = self.make_auth_header(ts, "api/stream", api_key, secret)
        return create_connection(url, header=headers)

    def disconnect(self, ws):
        try:
            ws.close()
        except WebSocketConnectionClosedException as e:
            pass
