import json
import logging
import sys
import time
from threading import Thread, current_thread

from decouple import config, Csv

from bitmax_exchange_api import BitmaxExchangeAPI, BitmaxExchangeWebsocketAPI
from informer_bot import send_message


class Informer:

    def __init__(self, api_key, secret, recepient):
        self.api_key = api_key
        self.secret = secret
        self.recepient = recepient
        self.API = BitmaxExchangeAPI()
        self.WSAPI = BitmaxExchangeWebsocketAPI()
        self.url = "https://bitmax.io"
        self.account_group = self.API.get_account_group(self.url, self.api_key, self.secret)
        self.ws_url = f"wss://bitmax.io/{self.account_group}/api/stream/BTMX-USDT"
        # self.ws_url = "wss://bitmax.io/api/public/ETH-BTC"
        logging.basicConfig(filename=config('LOG_FILE'), format='[%(asctime)s] %(name)s - %(levelname)s: %(message)s',
                            filemode='a', level=config('LOG_LEVEL', cast=int))


    def listen(self, ws):
        whoami = current_thread().name
        subscribe = """{
            "messageType":         "subscribe",
            "marketDepthLevel":    1,
            "recentTradeMaxCount": 1,
            "skipSummary": true,
            "skipBars": true
          }
        """
        try:
            logging.debug(f'{whoami}: Subscribing')
            ws.send(subscribe)
            logging.debug(f'{whoami}: Successfully')
        except Exception as e:
            logging.error(e)
            return

        running = True
        while running:
            try:
                start_t = 0
                if time.time() - start_t >= 30:
                    # Set a 30 second ping to keep connection alive
                    # logging.debug(f'{whoami}: Ping')
                    ws.ping("keepalive")
                    # logging.debug(f'{whoami}: Pong')
                    start_t = time.time()
                data = ws.recv()
                try:
                    # logging.debug(f'{whoami}: Trying to read message')
                    msg = json.loads(data)
                    # logging.debug(f'{whoami}: Message read')
                except:
                    # raise f"Failed to parse message a json: {data}"
                    logging.error(f"Failed to parse message a json: {data}")
                    running = False
            except ValueError as e:
                running = False
                print(e, file=sys.stderr)
                logging.error(e)
            except Exception as e:
                running = False
                print(e, file=sys.stderr)
                logging.error(e)
            else:
                # if msg['m'] == "order":
                if msg.get('m') not in ["depth", "bar", "pong", "summary", "marketTrades", "balance"]:
                    # message = f"ORDER CHANGE\nMessage type: {msg.get('m')}\nSymbol: {msg.get('s')}\nPrice: {msg.get('p')}\nQuantity: {msg.get('q')}\n" \
                    #     f"Side: {msg.get('side')}\nStatus: {msg.get('status')}"
                    message = f"ORDER CHANGE\nStatus: {msg.get('status')}\nSide: {msg.get('side')}\n" \
                        f"Symbol: {msg.get('s')}\nPrice: {msg.get('p')}\nQuantity: {msg.get('q')}\n" \
                        f"Message type: {msg.get('m')}"
                    print(f"received: {self.recepient}: {msg}")
                    print('------------------------------------')
                    logging.debug(f'{whoami}: Sending message')
                    send_message(self.recepient, message)
                    logging.debug(f'{whoami}: Message sent')
                else:
                    # print(msg)
                    # print(self.recepient, end=" ")
                    print(".", end=" ")

    def run(self):
        def loop(api_key, secret):
            while True:
                try:
                    ws = self.WSAPI.connect(self.ws_url, self.api_key, self.secret)
                    self.listen(ws)
                except Exception as err:
                    print(err, file=sys.stderr)
                finally:
                    self.WSAPI.disconnect(ws)

        whoami = current_thread().name
        thread = Thread(target=loop, args=(self.api_key, self.secret))
        thread.name = f"{whoami}.loop"
        thread.start()


if __name__ == '__main__':
    api_keys = config("API_KEY", cast=Csv())
    secrets = config('SECRET', cast=Csv())
    recepients = config('BOT_RECEIVER', cast=Csv())
    admin_tg = config('BOT_ADMIN')
    informer = dict()
    thread = dict()
    if not len(api_keys) == len(secrets) == len(recepients):
        send_message(admin_tg, 'error in .env file')
    else:
        for i, api_key, secret, recepient in zip(range(len(api_keys)), api_keys, secrets, recepients):
            print(i, recepient)
            informer[i] = Informer(api_key, secret, recepient)
            thread[i] = Thread(target=informer[i].run())
            thread[i].name = f"run.{recepient}"
            thread[i].start()