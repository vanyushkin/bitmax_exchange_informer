import logging
import sys

import requests
import telebot
from decouple import config, Csv

TOKEN = config('BOT_TOKEN')
RECEPIENTS = config('BOT_RECEIVER', cast=Csv())

bot = telebot.TeleBot(TOKEN, threaded=False)


def send_message(tg_id, msg):
    try:
        rez = bot.send_message(tg_id, msg)
    except Exception as err:
        print(f"Error sending message to {tg_id}: {err}", file=sys.stderr)
        logging.error(f"Error sending message to {tg_id}: {err}")
        return False
    print(f"Message: {msg} is sent")
    return True

@bot.message_handler()
def echo(message):
    bot.send_message(message.chat.id, message.chat.id)



if __name__ == '__main__':
    bot.polling(none_stop=True)